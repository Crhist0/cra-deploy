
import { Button } from '@mui/material';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { setLoggedUser } from '../../store/auth';
import { useAppDispatch } from '../../store/store';


export default function Login() {
  const dispatch = useAppDispatch()
  return (
<Box display='flex' flexDirection='column' gap='16px' alignItems='center' justifyContent='center' height={window.innerHeight - 64}>
    <Typography>Login</Typography>
    <Button onClick={()=>{
      dispatch(setLoggedUser(true))
    }}>Clique para logar</Button>
</Box>
  );
}
