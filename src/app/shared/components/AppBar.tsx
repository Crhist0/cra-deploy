import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { Link } from "react-router-dom";
import { useAppDispatch, useAppSelector } from '../../store/store';
import { logout } from '../../store/auth';

export default function ButtonAppBar() {
  const dispatch = useAppDispatch()
  const userLogged = useAppSelector(({auth})=>auth.userLogged)
  return (
      <>
        <Box sx={{ flexGrow: 1 }}>
          <AppBar position="static">
            <Toolbar sx={{gap: '20px'}}>
              <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                Deploy React
              </Typography>
            {userLogged && 
            <>
              <Link to='/' >
              <Button color='secondary' variant='contained'>Home</Button>
              </Link>
              <Link to='/about' >
              <Button color='secondary' variant='contained'>About</Button>
              </Link>
              <Link to='/contact' >
              <Button color='secondary' variant='contained'>Contact</Button>
              </Link>
              <Button color='error' variant='contained' onClick={()=>{
                dispatch(logout())
              }}>Logout</Button>
              </>}
            </Toolbar>
          </AppBar>
        </Box>
      </>
  );
}
