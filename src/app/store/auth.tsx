import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "./store";


export type AuthStore = {
    status: "idle" | "loading" | "failed";
    userLogged: boolean;
  };
  
  const initialState: AuthStore = {
    status: "idle",
    userLogged: false,
  };
  
  export const authSlice = createSlice({
    name: "auth",
    initialState,
    reducers: {
      setLoggedUser: (state, { payload }: PayloadAction<boolean>) => {
        state.userLogged = payload;
      },
      logout: () => {
        return initialState;
      },
    },
    extraReducers: ({ addCase }) => {

    },
  });
  
  export const { setLoggedUser, logout } = authSlice.actions;
  
  export const isAuthStateLoading = ({ auth }: RootState) => auth.status;
  export const isUserLogged = ({ auth }: RootState) => auth.userLogged;
  
  export default authSlice.reducer;