import { Outlet, useNavigate } from "react-router-dom";
import { useEffect } from "react";
import ButtonAppBar from "../shared/components/AppBar";
import { useAppSelector } from "../store/store";


export default function PrivateLayer() {
  const loggedUser = useAppSelector(({auth})=>auth.userLogged)

  const navigate = useNavigate();


  useEffect(() => {
    if (!loggedUser) {
      navigate("/login");
    }
  }, [loggedUser]);

  return <>
  <ButtonAppBar/>
  <Outlet />
  </>
}
