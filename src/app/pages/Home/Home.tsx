
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';



export default function Home() {
  return (
<Box display='flex' flexDirection='column' gap='16px' alignItems='center' justifyContent='center' height={window.innerHeight - 64}>
    <Typography>Home 🏠</Typography>
</Box>
  );
}
