
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';



export default function About() {
  return (
<Box display='flex' flexDirection='column' gap='16px' alignItems='center' justifyContent='center' height={window.innerHeight - 64}>
    <Typography>About 💁‍♂️</Typography>
</Box>
  );
}
