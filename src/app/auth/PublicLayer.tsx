import { Outlet, useNavigate } from "react-router-dom";
import { useEffect } from "react";
import { useAppSelector } from "../store/store";
import ButtonAppBar from "../shared/components/AppBar";


export default function PublicLayer() {
  const loggedUser = useAppSelector(({auth})=>auth.userLogged)

  const navigate = useNavigate();


  useEffect(() => {
    if (loggedUser) {
      navigate("/");
    }
  }, [loggedUser]);

  return <>
  <ButtonAppBar/>
  <Outlet />
  </>;
}
