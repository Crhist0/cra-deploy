import { BrowserRouter, Route, Routes } from "react-router-dom";
import PrivateLayer from "../auth/PrivateLayer";
import PublicLayer from "../auth/PublicLayer";
import About from "../pages/About/About";
import Contact from "../pages/Contact/Contact";
import Home from "../pages/Home/Home";
import Login from "../pages/Login/Login";

export default function Router() {
    return (<BrowserRouter>
    <Routes>
        <Route element={<PublicLayer/>}>
            <Route path="/login" element={<Login/>} />
        </Route>
        <Route element={<PrivateLayer/>}>
            <Route path="/" element={<Home/>} />
            <Route path="/about" element={<About/>} />
            <Route path="/contact" element={<Contact/>} />
        </Route>
    </Routes>
    </BrowserRouter>)
}